//panggil core module http
let http = require("http");
//panggil core module filesystem
const fs = require("fs");
//panggil core module path
const path = require("path");
//assign file json berisikan data random dari json generator
let rawData = fs.readFileSync("datas.json");
//assign data dalam bentuk array object
const datas = require("./datas.js");
//assign data yang telah diparsing data JSON yang diterima
const parsing = JSON.parse(rawData);
//destructuring object menjadi var dari sortData.js
const { data1, data2, data3, data4, data5} = require("./sortData.js");
//assign data parsing data JSON yang hendak dikirim
const data = JSON.stringify(datas);
//panggil modul dotenv
require("dotenv").config();
//destructuring object menjadi var dari .env
const { PORT = 8080 } = process.env;

//deklarasi function untuk render file html
function getHTML(htmlFileName) {
  const htmlFilePath = path.join(dir, htmlFileName);
  return fs.readFileSync(htmlFilePath, "utf-8");
}

//assign directory path untuk get html
const dir = path.join(__dirname, "resources/views/");

//deklarasi function untuk membaca file css
function getCSS(cssFileName) {
  const cssFilePath = path.join(aset, cssFileName);
  return fs.readFileSync(cssFilePath, { encoding: "utf8" });
}
//assign directory path untuk get css
const aset = path.join(__dirname, "resources/css/");

//deklarasi function untuk membaca file image
function getIMG(imgFileName) {
  const gambar = path.join(__dirname, "resources/img/");
  const imgFilePath = path.join(gambar, imgFileName);
  return imgFilePath
}

//deklarasi function request dan respon handler
function onRequest(req, res) {
  //cek kondisi berdasarkan request url
  switch (req.url) {
    case "/":
      //res.writehead() merupakan inbuilt property dari ‘http’ module yang mengirim a response header ke request yang masuk, argument pertama res.writeHead() adalah status code, 200 means that all is OK, argument kedua adalah object yang berisi the response headers.
      res.writeHead(200, { "Content-Type": "text/html" });
      //res.end() will end the response process. This method actually comes from Node core, specifically the response.end() method of http.ServerResponse. It is used to quickly end the response without any data.
      res.end(getHTML("homepage.html"));
      return;
    case "/data1":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data1(parsing));
      return;
    case "/data2":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data2(parsing));
      return;
    case "/data3":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data3(parsing));
      return;
    case "/data4":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data4(parsing));
      return;
    case "/data5":
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(data5(parsing));
      return;
    case "/about":
      res.writeHead(200, { "Content-Type": "text/html" });
      res.end(getHTML("about.html"));
      return;
    case "/style.css":
      res.writeHead(200, { "Content-type": "text/css" });
      // res.write is an inbuilt Application program Interface of the ‘http’ module which sends a chunk of the response body that is omitted when the request is a HEAD request. If this method is called and response.writeHead() has not been called, it will switch to implicit header mode and flush the implicit headers
      res.write(getCSS("/style.css"));
      res.end();
      return;
      case "/takut.jpg":
        fs.readFile(getIMG("/takut.jpg"), function(err, data) {
          if (err) throw err // Fail if the file can't be read.
    res.writeHead(200, {'Content-Type': 'image/jpeg'})
    res.end(data) 
  })
      return;
    default:
      res.writeHead(404);
      res.end(getHTML("404.html"));
      return;
  }
}

//instantiate http server
const server = http.createServer(onRequest);

// Jalankan server
server.listen(PORT, "0.0.0.0", () => {
  console.log("Server sudah berjalan, silahkan buka http://0.0.0.0:%d", PORT);
});
